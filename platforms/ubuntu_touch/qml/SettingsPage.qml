import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2
import Qt.labs.settings 1.0
import "components"

Page {
    id: settings
    
    anchors.fill: parent

    Rectangle {
        id: bkg
        anchors.fill: parent
        color: Suru.backgroundColor
    }

    header: PageHeader {
        title: i18n.tr("Settings")

        leadingActionBar.actions: [
            Action {
                iconName: pagesDrawer.isFullWidth ? 'back' : 'close'
                text: pagesDrawer.isFullWidth ? i18n.tr("Back") : i18n.tr("Close")
                onTriggered: pagesDrawer.close()
            }
        ]

        flickable: settingsPlugin
    }

    Flickable {
        id: settingsPlugin

        contentHeight: settingsColumn.height
        anchors.fill: parent

        Column {
            id: settingsColumn

            anchors {
                left: parent.left
                right: parent.right
            }
            
            ItemHeader {
                title: i18n.tr("General settings")
            }
            
            SwitchItem {
                titleText.text: i18n.tr("Sound")
                checked: gameSettings.sound
                onCheckedChanged: {
                    gameSettings.sound = checked
                    checked = Qt.binding(function(){return gameSettings.sound})
                }
            }
            
            SwitchItem {
                titleText.text: i18n.tr("Vibration")
                checked: gameSettings.vibrate
                onCheckedChanged: {
                    gameSettings.vibrate = checked
                    checked = Qt.binding(function(){return gameSettings.vibrate})
                }
            }
            
            SwitchItem {
                titleText.text: i18n.tr("Fullscreen")
                checked: gameSettings.fullscreen
                onCheckedChanged: {
                    gameSettings.fullscreen = checked
                    checked = Qt.binding(function(){return gameSettings.fullscreen})
                }
            }

            ItemHeader {
                title: i18n.tr("Emulator settings")
            }
            
            CheckBoxItem {
                id: autoLoadLatestCheckBox
                
                titleText.text: i18n.tr("Automatically load most recent save slot")
                bindValue: gameSettings.autoLoadLatest
                onCheckboxValueChanged: {
                    gameSettings.autoLoadLatest = checkboxValue
                }
            }
            
            ExpandableListItem {
                id: speedSettings

                listViewHeight: model.length * units.gu(6)
                model: [{ "value" : 2}, {"value" : 3}, {"value" : 4}, {"value" : 5}]
                title.text: i18n.tr("Speed multiplier")
                subText.text: {
                    for (var i = 0; i < model.length; i++) {
                        if (model[i].value == gameSettings.speedMultiplier) {
                            return "x " + model[i].value;
                        }
                    }

                    return "x 2";
                }

                delegate: ListItem {
                    height: units.gu(6)

                    ListItemLayout {
                        id: listitemlayout

                        title.text: "x " + modelData.value

                        Icon {
                            height: units.gu(2)
                            width: height
                            visible: speedSettings.subText.text == "x " + modelData.value
                            name: 'ok'
                            SlotsLayout.position: SlotsLayout.Last
                        }
                    }

                    onClicked: {
                        gameSettings.speedMultiplier = modelData.value;
                        speedSettings.toggleExpansion();
                    }
                }
            }
            
            ExpandableListItem {
                id: gbColorSetting

                listViewHeight: model.length * units.gu(6)
                model: [
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("GB White"), value: 'gb_white' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("GB Black"), value: 'gb_black' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("GB Gray"), value: 'gb_gray' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("GB Blue"), value: 'gb_blue' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("GB Purple"), value: 'gb_purple' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("System Color"), value: 'system_color' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Ambiance"), value: 'ambiance' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Suru Dark"), value: 'surudark' }
                ]
                title.text: i18n.tr("GB color")
                subText.text: {
                    for (var i = 0; i < model.length; i++) {
                        if (model[i].value == gameSettings.gbColor) {
                            return model[i].text;
                            break;
                        }
                    }

                    return i18n.tr("GB White");
                }

                delegate: ListItem {
                    height: units.gu(6)

                    ListItemLayout {
                        id: listitemlayout

                        title.text: modelData.text

                        Icon {
                            height: units.gu(2)
                            width: height
                            visible: gbColorSetting.subText.text == modelData.text
                            name: 'ok'
                            SlotsLayout.position: SlotsLayout.Last
                        }
                    }

                    onClicked: {
                        gameSettings.gbColor = modelData.value;
                        gbColorSetting.toggleExpansion();
                    }
                }
            }

            ExpandableListItem {
                id: paletteSetting

                listViewHeight: model.length * units.gu(6)
                model: [
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Original"), value: 'original' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Grayscale"), value: 'grayscale' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Brown"), value: 'brown' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Pastel Mix"), value: 'pastel_mix' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Blue"), value: 'blue' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Green"), value: 'green' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Red"), value: 'red' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Orange"), value: 'orange' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Dark Blue"), value: 'dark_blue' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Dark Green"), value: 'dark_green' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Dark Brown"), value: 'dark_brown' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Yellow"), value: 'yellow' },
                    // TRANSLATORS: This is a color palette option
                    { text: i18n.tr("Inverted"), value: 'inverted' }
                ]
                title.text: i18n.tr("GB Palette")
                subText.text: {
                    for (var i = 0; i < model.length; i++) {
                        if (model[i].value == gameSettings.dmgPalette) {
                            return model[i].text;
                            break;
                        }
                    }

                    return i18n.tr("Original");
                }

                delegate: ListItem {
                    height: units.gu(6)

                    ListItemLayout {
                        id: listitemlayout

                        title.text: modelData.text

                        Icon {
                            height: units.gu(2)
                            width: height
                            visible: paletteSetting.subText.text === modelData.text
                            name: 'ok'
                            SlotsLayout.position: SlotsLayout.Last
                        }
                    }

                    onClicked: {
                        gameSettings.dmgPalette = modelData.value;
                        paletteSetting.toggleExpansion();
                    }
                }
            }
            
            NavigationItem {
                id: inputNavigation
                
                titleText.text: i18n.tr("Input settings")
                subText.text: (gameSettings.enableTouch ? i18n.tr("Touch") + ", "  : "") + gameSettings.selectedInput
                
                anchors {
                    left: parent.left
                    right: parent.right
                }
                
                action: Action {
                    onTriggered: {
                        inputPage.visible = true
                    }
                }
            }
        }
    }
}
