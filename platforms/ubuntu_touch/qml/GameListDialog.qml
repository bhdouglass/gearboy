import QtQuick 2.9
import QtQuick.Controls 2.2

Dialog {
    id: gameListDialog
    
    readonly property bool isFullscreen: width === root.width && height === root.height
    
    property real maximumWidth: units.gu(70)
    property real preferredWidth: root.width
    
    property real maximumHeight: units.gu(90)
    property real preferredHeight: root.height > maximumHeight ? root.height / 1.5 : root.height
    
    width: preferredWidth > maximumWidth ? maximumWidth : preferredWidth
    height: preferredHeight > maximumHeight ? maximumHeight : preferredHeight

    x: (root.width - width) / 2
    y: root.width > maximumWidth ? (root.height - height) / 2 : (root.height - height)
    
    parent: ApplicationWindow.overlay
    padding: 0
    topPadding: 0
    clip: true

    modal: true
    
    onOpened: {
        emu.pause()
    }
    
    onClosed: {
        gameListPage.visible = false
        emu.play()
    }
    
    onAboutToShow: {
        gameListPage.visible = true
    }
}
