import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    id: cornerButton
    
    signal clicked()
    
    property string corner: "right"
    property string iconName
    property alias subscriptText: slotLabel.text
    
    anchors {
        verticalCenter: parent.bottom
        horizontalCenter: corner === "right" ? parent.right : parent.left
    }
    
    opacity: mouseArea.pressed ? 0.5 : 1
    
    Rectangle {
        id: shaded_corner
        width: units.gu(16)
        height: units.gu(28)
        color: applicationWindow[gameSettings.gbColor + "_accent"] ? applicationWindow[gameSettings.gbColor + "_accent"] : Qt.darker(gb_white, 1.05)
        rotation: corner === "right" ? 50 : -50

        anchors {
            verticalCenter: parent.bottom
            horizontalCenter: corner === "right" ? parent.right : parent.left
        }
    }
    
    Icon {
        name: cornerButton.iconName
        color: gb_gray
        width: units.gu(4)
        height: units.gu(4)
        
        anchors {
            bottom: parent.bottom
            right: corner === "right" ? parent.right : undefined
            left: corner === "right" ? undefined : parent.left
            margins: units.gu(1.5)
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            onClicked: {
                root.click();
                cornerButton.clicked()
            }
        }
        
        Label {
            id: slotLabel
            
            visible: text
            color: gb_gray
            textSize: Label.Large
            font.bold: true
            
            anchors {
                top: parent.verticalCenter
                right: corner === "right" ? parent.left : undefined
                left: corner === "right" ? undefined : parent.right
            }
            
        }
    }
}
