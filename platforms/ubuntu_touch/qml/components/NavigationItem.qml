import QtQuick 2.9
import Ubuntu.Components 1.3

ListItem {
    id: navigationItem

    property alias titleText: listItemLayout.title
    property alias subText: listItemLayout.subtitle
    property string iconName
    property alias iconColor: icon.color

    width: parent.width
    divider.visible: false

    ListItemLayout {
        id: listItemLayout

        ProgressionSlot{}

        Icon {
            id: icon
            name: iconName
            SlotsLayout.position: SlotsLayout.Leading
            width: units.gu(3)
            visible: iconName !== "" ? true : false
        }
    }
}
