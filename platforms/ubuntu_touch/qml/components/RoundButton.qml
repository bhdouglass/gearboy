import QtQuick 2.9
import Ubuntu.Components 1.3

MouseArea {
    id: roundButton
    
    property string buttonType: "neutral"
    property color labelColor: "#EDEDED"
    property color fillColor: switch (buttonType) {
            case "positive":
                theme.palette.normal.positive
                break;
            case "negative":
                theme.palette.normal.negative
                break;
            case "neutral":
                Qt.darker("#ABABAB", 1.0)
                break;
            default:
                Qt.darker("#ABABAB", 1.0)
                break;
    }
    
    property color borderColor: Qt.darker("#E1E1E1", 1.075)

    property real outline: width / 10
    property alias text: label.text
    property alias iconName: icon.name
    
    onClicked: root.click();

    Rectangle {
        anchors.fill: parent
        radius: width / 2
        color: borderColor
        
        opacity: roundButton.pressed ? 0.5 : 1

        Rectangle {
            anchors {
                fill: parent
                margins: units.gu(0.2)
            }

            color: fillColor
            border {
                color: borderColor
                width: outline
            }
            radius: width / 2

            Label {
                id: label

                visible: text
                anchors.centerIn: parent

                color: labelColor
                fontSize: "x-large"
            }

            Icon {
                id: icon

                visible: iconName
                anchors.centerIn: parent

                color: labelColor
                width: parent.width / 2
                height: width
            }
        }
    }
}
