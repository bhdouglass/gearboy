import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Themes.Ambiance 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2
import "components"

Page {
    id: externalInputPage

    property alias title: pageHeader.title
    property bool editMode: false
    property var qtKeysData: []
    property var inputData

    anchors.fill: parent

    header: PageHeader {
        id: pageHeader

        leadingActionBar.actions: [
            Action {
                iconName: 'back'
                text: i18n.tr("Back")
                onTriggered: externalInputPage.visible = false
            }
        ]

        trailingActionBar.actions: [
            Action {
                iconName: 'save'
                text: i18n.tr("save")
                onTriggered: {
                    keyboard.target.commit()

                    if (save()) {
                        var tooltipText = editMode ? i18n.tr("Input saved") : i18n.tr("Input added")
                        externalInputPage.visible = false
                        notifyTooltip.display(tooltipText, "BOTTOM")
                    } else {
                        notifyTooltip.display(i18n.tr("Invalid or duplicate name. Please change"), "BOTTOM")
                    }
                }
            }
        ]
    }

    onVisibleChanged: {
        if (visible) {
            initializeValues()
            flickable.contentY = 0
        }
    }

    Component.onCompleted: {
        getQtKeysData(function(success, jsonData) {
                if (success) {
                    qtKeysData = jsonData.keys
                }
            })
    }

    function initializeValues() {
        textFieldName.text = inputData ? inputData.name : ""
        inputTypeExpandable.selectedIndex = getIndex(inputTypeExpandable.model, inputData ? inputData.type : "keyboard", "value")
        listView.keyMap = inputData.keymap


        // IMPORTANT: This resets the model otherwise there will be sync issues with the values.
        listView.model = []
        listView.model = [
                {"name": i18n.tr("Up"), "id": "up", "value": listView.keyMap.up || listView.keyMap.up >= 0 ? listView.keyMap.up.toString() : "-1"},
                {"name": i18n.tr("Down"), "id": "down", "value": listView.keyMap.down || listView.keyMap.down >= 0 ? listView.keyMap.down.toString() : "-1"},
                {"name": i18n.tr("Left"), "id": "left", "value": listView.keyMap.left || listView.keyMap.left >= 0 ? listView.keyMap.left.toString() : "-1"},
                {"name": i18n.tr("Right"), "id": "right", "value": listView.keyMap.right || listView.keyMap.right >= 0 ? listView.keyMap.right.toString() : "-1"},
                {"name": i18n.tr("A"), "id": "a", "value": listView.keyMap.a || listView.keyMap.a >= 0 ? listView.keyMap.a.toString() : "-1"},
                {"name": i18n.tr("B"), "id": "b", "value": listView.keyMap.b || listView.keyMap.b >= 0 ? listView.keyMap.b.toString() : "-1"},
                {"name": i18n.tr("Select"), "id": "select", "value": listView.keyMap.select || listView.keyMap.select >= 0 ? listView.keyMap.select.toString() : "-1"},
                {"name": i18n.tr("Start"), "id": "start", "value": listView.keyMap.start || listView.keyMap.start >= 0 ? listView.keyMap.start.toString() : "-1"},
                {"name": i18n.tr("Pause"), "id": "pause", "value": listView.keyMap.pause || listView.keyMap.pause >= 0 ? listView.keyMap.pause.toString() : "-1"},
                {"name": i18n.tr("Quick Save"), "id": "quicksave", "value": listView.keyMap.quicksave || listView.keyMap.quicksave >= 0 ? listView.keyMap.quicksave.toString() : "-1"},
                {"name": i18n.tr("Quick Load"), "id": "quickload", "value": listView.keyMap.quickload || listView.keyMap.quickload >= 0 ? listView.keyMap.quickload.toString() : "-1"},
                {"name": i18n.tr("Previous Save Slot"), "id": "prevslot", "value": listView.keyMap.prevslot || listView.keyMap.prevslot >= 0 ? listView.keyMap.prevslot.toString() : "-1"},
                {"name": i18n.tr("Next Save Slot"), "id": "nextslot", "value": listView.keyMap.nextslot || listView.keyMap.nextslot >= 0 ? listView.keyMap.nextslot.toString() : "-1"},
                {"name": i18n.tr("Fast Forward"), "id": "fast", "value": listView.keyMap.fast || listView.keyMap.fast >= 0 ? listView.keyMap.fast.toString() : "-1"},
                {"name": i18n.tr("Mute"), "id": "mute", "value": listView.keyMap.mute || listView.keyMap.mute >= 0 ? listView.keyMap.mute.toString() : "-1"}
            ]
    }

    function saveValues() {
        inputData.name = textFieldName.text
        inputData.type = inputTypeExpandable.selectedValue
        inputData.keymap = listView.keyMap
    }

    function save() {
        var oldName = inputData.name
        var newName = textFieldName.text

        if (editMode) {
            return editInput(oldName, newName)
        } else {
            return addInput(newName)
        }
    }

    function getQtKeysData(callback){
        var xhr = new XMLHttpRequest();
        var resultData


        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    console.log("JSON fetch success")
                    resultData = JSON.parse(xhr.responseText)
                    callback(true, resultData)
                }
                else {
                    console.log('Failed to fetch JSON');
                    callback(false)
                }
            }
        };

        xhr.open('GET', Qt.resolvedUrl("data/qt_keys.json"), true);
        xhr.send();

    }

    function getIndex(arrayObj, value, attribute){
        var index = arrayObj.map(function(e) { return e[attribute]; }).indexOf(value)
        return index
    }

    function addInput(name){
        var temp = gameSettings.externalInputs.slice()
        var matches = root.getMatchesInArray(temp, name, "name")

        if ( !matches.length && name !== ""){
            saveValues()
            temp.push(inputData)
            gameSettings.externalInputs = temp.slice()
            return true
        }else{
            return false
        }
    }

    function editInput(name, newName){
        var temp = gameSettings.externalInputs.slice()
        var currentIndex = getIndex(temp, name, "name")
        var newNameIndex = getIndex(temp, newName, "name")

        if ((currentIndex == newNameIndex || newNameIndex == -1) && newName !== "") {
            saveValues()
            temp[currentIndex] = inputData
            gameSettings.externalInputs = temp.slice()
            if (gameSettings.selectedInput === name) {
                gameSettings.selectedInput = newName
            }
            return true
        } else {
            return false
        }
    }

    Connections {
        id: keyboard
        target: Qt.inputMethod
    }

    Rectangle {
        id: bkg
        anchors.fill: parent
        color: Suru.backgroundColor
    }

    Column {
        id: headerColumn

        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
        }

        TextField {
            id: textFieldName

            height: units.gu(8)
            font.pixelSize: units.gu(4)

            anchors {
                left: parent.left
                right: parent.right
            }

            placeholderText: i18n.tr("Enter input name")
            hasClearButton: true
        }

        ExpandableListItem {
            id: inputTypeExpandable

            readonly property string selectedValue: model[selectedIndex] ? model[selectedIndex].value : ""
            property int selectedIndex

            listViewHeight: model.length * units.gu(6)
            title.text: model[selectedIndex].text
            title.font.pixelSize: units.gu(3)
            leadingIcon:  model[selectedIndex].icon

            model: [
                { text: i18n.tr("Keyboard"), value: 'keyboard', icon: 'input-keyboard-symbolic' },
                { text: i18n.tr("Gamepad"), value: 'gamepad', icon: 'input-gaming-symbolic' },
                { text: i18n.tr("Others"), value: 'others', icon: 'other-actions' }
            ]

            delegate: ListItem {
                height: units.gu(6)

                ListItemLayout {
                    id: listitemlayout

                    title.text: modelData.text

                    Icon {
                        height: units.gu(3)
                        width: height
                        name: modelData.icon
                        SlotsLayout.position: SlotsLayout.Leading
                    }

                    Icon {
                        height: units.gu(2)
                        width: height
                        visible: inputTypeExpandable.selectedIndex === index
                        name: 'ok'
                        SlotsLayout.position: SlotsLayout.Last
                    }
                }

                onClicked: {
                    inputTypeExpandable.selectedIndex = index;
                    inputTypeExpandable.toggleExpansion();
                }
            }
        }

        Label {
            height: units.gu(7)
            text: i18n.tr("Some gamepads may not be detected correctly due to system limitations")
            wrapMode: Text.Wrap
            verticalAlignment: Text.AlignVCenter

            anchors {
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }
        }
    }

    ScrollView {
         id: scrollView

         anchors {
            top: headerColumn.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Flickable {
            id: flickable

            interactive: true
            clip: true
            contentHeight: contentColumn.height
            anchors.fill: parent

            Column {
                id: contentColumn

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                Repeater {
                    id: listView

                    property var keyMap

                    clip: true

                    function clearDuplicateKeys(value, nativeValue, index) {
                        for(var i = 0; i < listView.count; i++) {
                            if ((itemAt(i).keyValue > 0 && itemAt(i).keyValue == value)
                                    || (itemAt(i).keyValue == 0 && itemAt(i).nativeScanCode == nativeValue)) {
                                if (i >= 0 && index !== i) {
                                    keyMap[model[i].id] = "-1"
                                    itemAt(i).keyValue = -1
                                    itemAt(i).nativeScanCode = -1
                                }
                            }
                        }
                    }

                    delegate: ListItem{
                        id: keyListItem

                        readonly property var qtKeyMatch: root.getMatchesInArray(externalInputPage.qtKeysData, keyValue, "value")
                        property bool detectKey: false
                        property int keyValue: modelData.value.indexOf(root.scanCodePrefix) >= 0 ? 0
                                        : modelData.value
                        property int nativeScanCode: keyValue == 0 ? modelData.value.replace(root.scanCodePrefix, "")
                                        :  0
                        property string qtKeyName:  keyValue == 0 && nativeScanCode > 0 ? i18n.tr("Key") + " " + nativeScanCode
                                            : qtKeyMatch.length > 0 ? qtKeyMatch[0].name : ""
                        property string keyText: qtKeyName ? qtKeyName : keyValue >= 0 ? keyValue : ""

                        onClicked:  detectKey = !detectKey

                        onKeyValueChanged: {
                            if (keyValue > 0 || keyValue == -1) {
                                listView.keyMap[modelData.id] = keyValue
                            }
                        }

                        onNativeScanCodeChanged: {
                            if (keyValue == 0) {
                                // Store as scan code instead
                                listView.keyMap[modelData.id] = root.scanCodePrefix + nativeScanCode
                            }
                        }

                        onActiveFocusChanged: {
                            if (!activeFocus) {
                                detectKey = false
                            }
                        }

                        Keys.onPressed: {
                            if (detectKey) {
                                event.accepted = true
                            }
                        }

                        Keys.onReleased: {
                            if (detectKey) {
                                keyValue = event.key
                                nativeScanCode = event.nativeScanCode
                                listView.clearDuplicateKeys(keyValue, nativeScanCode, index)
                                detectKey = false
                                event.accepted = true
                            }
                        }

                        InverseMouseArea {
                            enabled: keyListItem.detectKey
                            anchors.fill: parent
                            topmostItem: true
                            acceptedButtons: Qt.LeftButton
                            onPressed: keyListItem.detectKey = false
                        }

                        color: keyListItem.detectKey ? theme.palette.normal.selection : theme.palette.normal.background

                        ListItemLayout {
                            title.text: modelData.name
                            title.color: keyListItem.detectKey ? theme.palette.normal.selectionText : theme.palette.normal.backgroundText

                            Rectangle {
                                color: keyListItem.keyValue >= 0 ? theme.palette.normal.foreground : theme.palette.selected.foreground
                                implicitWidth: units.gu(20)
                                implicitHeight: units.gu(5)

                                SlotsLayout.position: SlotsLayout.Trailing
                                radius: units.gu(1)

                                Label {
                                    id: valueLabel

                                    text: keyListItem.detectKey ? i18n.tr("Press a key") : keyListItem.keyText
                                    color: keyListItem.keyValue >= 0 ? theme.palette.selected.foregroundText : theme.palette.normal.foregroundText
                                    anchors.centerIn: parent
                                    font.bold: true

                                    state: keyListItem.detectKey ? "blinking" : "steady"

                                    states: [
                                        State {
                                            name: "blinking"
                                            when: keyListItem.detectKey
                                        }
                                    ]

                                    transitions: [
                                        Transition {
                                            from: "steady"
                                            to: "blinking"

                                            SequentialAnimation {
                                                loops: Animation.Infinite
                                                PropertyAnimation {target: valueLabel; property: "opacity"; from: 1.0; to: 0.0; duration: UbuntuAnimation.SlowDuration }
                                                PropertyAnimation {target: valueLabel; property: "opacity"; from: 0.0; to: 1.0; duration: UbuntuAnimation.SlowDuration }
                                            }
                                        },
                                        Transition {
                                            from: "blinking"
                                            to: "steady"
                                                PropertyAnimation {target: valueLabel; property: "opacity"; to: 1.0; duration: UbuntuAnimation.SnapDuration }
                                        }
                                    ]
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}
