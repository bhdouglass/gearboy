import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import "components"

QQC2.Drawer {
    id: drawer
    
    property alias model: listView.model
    property int latestSlot: -1
    
    readonly property real minimumWidth: units.gu(35)
    readonly property real preferredWidth: root.width * 0.25
    
    width: preferredWidth < minimumWidth ? minimumWidth : preferredWidth
    height: root.height
    
    background: Rectangle{color: theme.palette.normal.background}
    
    onClosed: {
        emu.play()
    }
    
    onOpened: {
        emu.pause()
        listView.positionViewAtEnd()
    }
    
    ListView {
        id: listView

        focus: true
        clip: true
        currentIndex: -1
        verticalLayoutDirection: ListView.BottomToTop
        
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: quickButtonsRow.top
            bottomMargin: units.gu(5)
        }
        
        delegate: ListItem{
            readonly property bool islatest: drawer.latestSlot == modelData.slot
            
            selected: root.currentSlot === modelData.slot
            
            color: selected ? theme.palette.selected.background : theme.palette.normal.background
            
            onClicked: {
                root.currentSlot = modelData.slot
            }
            
            ListItemLayout {
                title.text: i18n.tr('Save Slot') + " " + modelData.slot
                title.font.bold: selected
                title.color: selected ? theme.palette.selected.backgroundText : theme.palette.normal.backgroundText
                subtitle.text: modelData.dateLabel
                padding.trailing: islatest ? 0 : units.gu(1)
                
                Rectangle {
                    visible: islatest
                    color: theme.palette.normal.positive
                    implicitHeight: parent.height
                    implicitWidth: units.gu(0.5)
                    SlotsLayout.position: SlotsLayout.Last
                    SlotsLayout.overrideVerticalPositioning: true
                    SlotsLayout.padding.trailing: islatest ? 0 : units.gu(1)
                }
                
                Icon {
                    id: icon
                    visible: selected
                    implicitWidth: units.gu(3)
                    implicitHeight: implicitWidth
                    name: "ok"
                    color: selected ? theme.palette.selected.backgroundText : theme.palette.normal.backgroundText
                    
                    SlotsLayout.position: SlotsLayout.Trailing
                 }
            }
        }
    }
    
    Row {
        id: quickButtonsRow
        
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: units.gu(2)
        }
        
        spacing: units.gu(4)
        
        RoundButton {
            id: quickSaveButton

            buttonType: "positive"
            iconName: 'save'
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                root.quickSave()
                drawer.close()
            }
        }
        
        RoundButton {
            id: quickLoadButton

            buttonType: "neutral"
            iconName: 'document-open'
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                root.quickLoad()
                drawer.close()
            }
        }
    }
}
