import QtQuick 2.9
import Qt.labs.folderlistmodel 2.1

FolderListModel {
    id: savesFolderList
    
    readonly property string saveSlotPath: root.savesDir + root.loadedROM + ".state"
    
    showDotAndDotDot: false
    showHidden: false
    rootFolder: root.savesDir
    folder: root.savesDir
    showDirs: false
    nameFilters: ["*.state*"]
    
    onCountChanged: {
        if (count > 0) {
            updateSaveData()
        }
    }
    
    function formatDateTime(date) {
        var locale = Qt.locale()
        var formattedDate = date.toLocaleString(locale, Locale.ShortFormat)
        
        var dateCompare = new Date(date).setHours(0,0,0,0)
        var today = new Date()
        var todayCompare = new Date().setHours(0,0,0,0)
        var yesterday =  new Date()
        yesterday.setDate(yesterday.getDate()-1)
        yesterday.setHours(0,0,0,0)
        
        const diffTime = Math.abs(today - dateCompare);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        
        if (+dateCompare === +todayCompare) {
            const diffMins = Math.floor((Math.abs(today - date)/1000)/60);
            if (diffMins > 0) {
                if (diffMins < 60){
                    return formattedDate + ", " + i18n.tr("a minute ago", "%1 minutes ago", diffMins).arg(diffMins)
                } else {
                    const diffHours = Math.floor(diffMins/ 50)
                    return formattedDate + ", " + i18n.tr("an hour ago", "%1 hours ago", diffHours).arg(diffHours)
                }
            } else {
                return formattedDate + ", " + i18n.tr("Just now")
            }
        }
        else if (+dateCompare === +yesterday) {
            return formattedDate + ", " + i18n.tr("Yesterday")
        } else {
            return formattedDate + ", " + i18n.tr("%1 days ago").arg(diffDays)
        }
        
        return formattedDate
    }
    
    function refresh() {
        folder = ""
        folder = root.savesDir
    }
    
    function updateSaveData(callback) {
        var saveIndex
        var saveDate
        var latestSlot = -1
        var prevLatestDate
        var arr = menuDrawer.modelData.slice()
        var arrLength = arr.length;
        
        arr.forEach(function (item, i) {
            item.dateLabel = ""
            
            saveIndex = indexOf(saveSlotPath + item.slot)
            
            if (saveIndex > -1) {
                saveDate = get(saveIndex,"fileModified")
                
                if (saveDate) {
                    item.dateLabel = formatDateTime(saveDate)
                    
                    if (!prevLatestDate || prevLatestDate < saveDate) {
                        latestSlot = item.slot
                        prevLatestDate = saveDate
                    }                        
                }
            }
        });
        
        menuDrawer.latestSlot = latestSlot

        menuDrawer.modelData = arr.slice()
        
    }
}
