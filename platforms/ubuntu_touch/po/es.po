# Spanish translation for gearboy
# Copyright (C) 2019
# This file is distributed under the same license as the gearboy package.
# Krakakanok Krakakanok@protonmail, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-06 04:00+0000\n"
"PO-Revision-Date: 2019-08-31 11:50+0200\n"
"Last-Translator: Krakakanok <Krakakanok@protonmail.com>\n"
"Language-Team: UBports Spanish Team\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:24
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:104
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameSettingsPage.qml:35
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:63
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:24
msgid "Back"
msgstr "Atrás"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:32
msgid "save"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:37
#, fuzzy
msgid "Input saved"
msgstr "Ajustes"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:37
msgid "Input added"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:41
msgid "Invalid or duplicate name. Please change"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:72
msgid "Up"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:73
msgid "Down"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:74
msgid "Left"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:75
msgid "Right"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:76
msgid "A"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:77
msgid "B"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:78
msgid "Select"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:79
msgid "Start"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:80
msgid "Pause"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:81
msgid "Quick Save"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:82
msgid "Quick Load"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:83
msgid "Previous Save Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:84
msgid "Next Save Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:85
msgid "Fast Forward"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:86
msgid "Mute"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:199
msgid "Enter input name"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:215
msgid "Keyboard"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:216
msgid "Gamepad"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:217
msgid "Others"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:253
msgid "Some gamepads may not be detected correctly due to system limitations"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:321
msgid "Key"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/ExternalInputPage.qml:387
msgid "Press a key"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:99
msgid "ROMs"
msgstr "ROMs"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:104
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:24
msgid "Close"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:112
msgid "Import ROM"
msgstr "Importar ROM"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:146
msgid ""
"No ROMs available.\n"
"Tap the plus to add your own!"
msgstr ""
"No hay ninguna ROM disponible.\n"
"¡Toque el más para añadir la que quiera!"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:166
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:269
msgid "Delete"
msgstr "Borrar"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameListPage.qml:177
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:282
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:19
msgid "Settings"
msgstr "Ajustes"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameSettingsPage.qml:43
msgid "Play"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/GameSettingsPage.qml:73
msgid "Cheat Codes"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:24
msgid "Edit input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:27
msgid "Add new input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:58
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:260
#, fuzzy
msgid "Input settings"
msgstr "Ajustes"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:71
msgid "Add"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:104
msgid "On-screen input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:110
msgid "Enable on-screen input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:125
msgid "Hide all on-screen buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:126
msgid "Tap screen to toggle visibility of the buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:137
msgid "Automatically disable on-screen buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:138
msgid "When external input is detected"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:163
msgid "Opacity"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:193
msgid "External input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:199
msgid "Fast forward only while key is pressed"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/InputPage.qml:200
msgid "Otherwise, it works like a toggle"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:244
msgid "ROM failed to load"
msgstr "La ROM no pudo cargarse"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:261
#, fuzzy
msgid "Sound muted"
msgstr "Sonido"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:261
#, fuzzy
msgid "Sound enabled"
msgstr "Sonido"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:280
msgid "State saved in Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:287
msgid "State loaded from Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:528
msgid "OPEN ROM…"
msgstr "ABRIR ROM…"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:591
msgid "SELECT"
msgstr "SELECT"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/main.qml:620
msgid "START"
msgstr "START"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SaveFolderModel.qml:40
#, qt-format
msgid "a minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SaveFolderModel.qml:43
#, qt-format
msgid "an hour ago"
msgid_plural "%1 hours ago"
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SaveFolderModel.qml:46
msgid "Just now"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SaveFolderModel.qml:50
msgid "Yesterday"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SaveFolderModel.qml:52
#, qt-format
msgid "%1 days ago"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:47
#, fuzzy
msgid "General settings"
msgstr "Ajustes"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:51
msgid "Sound"
msgstr "Sonido"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:60
msgid "Vibration"
msgstr "Vibración"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:69
msgid "Fullscreen"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:78
#, fuzzy
msgid "Emulator settings"
msgstr "Ajustes"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:84
msgid "Automatically load most recent save slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:96
msgid "Speed multiplier"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:137
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:162
msgid "GB White"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:139
msgid "GB Black"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:141
msgid "GB Gray"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:143
#, fuzzy
msgid "GB Blue"
msgstr "Azul"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:145
#, fuzzy
msgid "GB Purple"
msgstr "Paleta GB"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:147
msgid "System Color"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:149
msgid "Ambiance"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:151
msgid "Suru Dark"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:153
msgid "GB color"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:195
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:230
msgid "Original"
msgstr "Original"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:197
msgid "Grayscale"
msgstr "Escala de grises"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:199
msgid "Brown"
msgstr "Marrón"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:201
msgid "Pastel Mix"
msgstr "Mezcla de colores pastel"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:203
msgid "Blue"
msgstr "Azul"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:205
msgid "Green"
msgstr "Verde"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:207
msgid "Red"
msgstr "Rojo"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:209
msgid "Orange"
msgstr "Naranja"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:211
msgid "Dark Blue"
msgstr "Azul oscuro"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:213
msgid "Dark Green"
msgstr "Verde oscuro"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:215
msgid "Dark Brown"
msgstr "Marrón oscuro"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:217
msgid "Yellow"
msgstr "Amarillo"

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:219
msgid "Inverted"
msgstr "Invertido"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:221
msgid "GB Palette"
msgstr "Paleta GB"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/qml/SettingsPage.qml:261
msgid "Touch"
msgstr ""

#, fuzzy
#~ msgid "GB Green"
#~ msgstr "Verde"

#, fuzzy
#~ msgid "Gameboy Green"
#~ msgstr "Verde oscuro"

#~ msgid "Show power and restart buttons"
#~ msgstr "Mostrar botones de encendido y reinicio"

#, fuzzy
#~ msgid "Settings for"
#~ msgstr "Ajustes"
