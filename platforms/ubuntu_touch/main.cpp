#include <QApplication>
#include <QQuickView>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QQuickStyle>
#include <QDebug>

#include "GBEmulator.h"
#include "Files.h"


int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("Gearboy");
    QGuiApplication::setOrganizationName("gearboy.bhdouglass");
    QGuiApplication::setApplicationName("gearboy.bhdouglass");

    // Disabled for now since some devices like the Pinephone has issue with auto-scaling
    // QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<GBEmulator>("GearBoy", 1, 0, "GearBoyEmulator");
	qmlRegisterSingletonType<Files>("GearBoy", 1, 0, "Files", [](QQmlEngine*, QJSEngine*) -> QObject* { return Files::instance(); });

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    int result = app.exec();
	EmulationRunner::waitAll();
	return result;
}
